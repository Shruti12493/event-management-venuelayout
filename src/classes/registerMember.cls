public class registerMember {
    @AuraEnabled
    public static void memberRegistration(String conactId, String venueId, String memberName){
        Event_Registration__c eventRegistration = new Event_Registration__c();
        eventRegistration.Contact__c = conactId;
        eventRegistration.Venue__c = venueId;
        
        Insert eventRegistration;
    }
}