({
	drawDefaultVenueLayout : function(component) {
        
        debugger;
        var globalID = component.getGlobalId();
        console.log(component.getGlobalId());
        var numberOfSeats = component.get("v.wrapperObj.NumberOfSeats");
 		var totalSeatCounter = 1;
        // classroom
        
        var capacity = component.get("v.wrapperObj.NumberOfSeats");//50;
        var seatsPerRow = component.get("v.wrapperObj.SeatsPerRow");//10;
        var numberOfRows = Math.round(capacity/seatsPerRow);
        var seats = new Array(seatsPerRow) || 1;
        var rows = new Array(numberOfRows) || 1;
        var columns = 1;


         /* Edit Layout
           Added by: Ankush
           Date    : 13/07/2017 */
        
        var obj =  component.get("v.wrapperObj");
        var stion = component.get("v.section");
        console.log("obj+++++++" +obj);
        var parentdiv = document.getElementById(globalID); 
        parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
        var sObjectEvent = $A.get("e.c:EditLayoutEvent");
       		sObjectEvent.setParams({
            	"message" : obj
            });
        sObjectEvent.fire();
        }));
        
        
       /*Edit Layout End*/ 
        
        
        // booth
        
        var capacity = component.get("v.wrapperObj.NumberOfSeats");//50;
        var seatsPerRow = component.get("v.wrapperObj.SeatsPerRow");//10;
        var numberOfRows = Math.round(capacity/seatsPerRow);
        //var row = 1;
        for(var cap=0;cap < numberOfRows;cap++){
            var audienceTemplate =  $('#audience-layout-template').html();
            var context = $(audienceTemplate);
      		var rowTemplate = document.getElementById(globalID + '_seat-template');
            var rowLi = document.createElement('li');
            rowLi.id = globalID + '_rowLi';
            var rowUl = document.createElement('ul');
            rowUl.id = globalID + '_rowUl';
            rowUl.style.margin = '3px';
            rowTemplate.appendChild(rowLi);
            
            for(var row=0;row<seatsPerRow;row++){
                    if(totalSeatCounter <= numberOfSeats ){
    					
                    var seatLi = document.createElement('li');
				    seatLi.className = 'chair';
                    rowUl.appendChild(seatLi);
                     $(seatLi).on('dragover', function (event) {
                     console.log('ondragover0 called...');
                     event.preventDefault();
                });
                
                $(seatLi).on('drop', function (event) {
                    var status = $(this).attr('data');
                    if(status != 'booked'){
                    var listitem = event.originalEvent.dataTransfer.getData("obj");
                    $(this).css('background-color','#D5A69B');
                    $(this).attr('data','booked');
                    console.log("Inside drop =====> "+listitem); 
                  //  alert("element dropped here    "+listitem);
                    var seatNo = $(this).find('label').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat = tableNo + '_'+ seatNo;
                 //alert("seatNo********"+seat);
    				var obje = JSON.parse(listitem);
                    var contactId = obje.Id;
                    var sObjectEvent1 = component.getEvent("getAssignSeat");
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo})
                    sObjectEvent1.fire();
                        }
                });
                        totalSeatCounter++;
                    }
                }
            rowLi.appendChild(rowUl);
            $("ul[id='"+globalID+"_seat-template"+"']").append(rowLi);
          //  row++;
        }
    
    }
    
})