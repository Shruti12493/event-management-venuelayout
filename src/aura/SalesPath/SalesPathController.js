({
	 callForRecordId : function(component, event, helper) {
        debugger;
        var action = component.get("c.getPicklistvalues");
          var recID = component.get("v.recordId");
         action.setParams({
            "recdId": recID
             })
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
         debugger;
         var stringItems = response.getReturnValue();

            var state = response.getState();
            if (state === "SUCCESS"){
                debugger;
                component.set("v.presentList", stringItems.current);
                component.set("v.pastList", stringItems.past);
                component.set("v.futureList", stringItems.future);
                component.set("v.objectName",stringItems.objectName[0]);
                component.set("v.fieldName",stringItems.fieldName[0]);
                 
            }
            
        });
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    },
    
    clickOnSalesPath : function(component, event, helper) {
        debugger;
        var selectedItem = event.currentTarget;
        var inputsel = selectedItem.dataset.record;
        component.set("v.valueToUpdate", inputsel);
       
     },
    
    clickToSave: function(component, event, helper) {
       debugger;
        var action = component.get("c.saveInAccount");
        var valueToUpdate = component.get("v.valueToUpdate");
        var recID = component.get("v.recordId");
        var objName = component.get("v.objectName");
        var fldName = component.get("v.fieldName");
         action.setParams({
            "valueToUpdate": valueToUpdate,
             "recdId": recID,
             "objectName":objName,
             "fieldName":fldName
             })
          action.setCallback(this, function(response) {
         debugger;
         var stringItems = response.getReturnValue();
              var state = response.getState();
            if (state === "SUCCESS"){
              var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "Success!",
                           "title": "Success!",
                          "message": "Record updated Successfully",
                               });
                     toastEvent.fire();
            }
        
    });
         $A.enqueueAction(action);
    }
    
})